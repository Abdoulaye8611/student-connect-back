//
//package com.students.infrastruture.config;
//
//import javax.annotation.PostConstruct;
//
//
//import com.students.dao.MarkRepository;
//import org.joda.time.DateTime;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.students.dao.StudentRepository;
//import com.students.dao.TeacherRepository;
//import com.students.dao.WorkRepository;
//
//import com.students.entities.Course;
//import com.students.entities.Student;
//import com.students.entities.Teacher;
//import com.students.entities.Work;
//import com.students.entities.Mark;
//
//
//
//@Configuration
//public class DBInit {
//
//	@Autowired
//	private StudentRepository studentRepository;
//
//	@Autowired
//	private TeacherRepository teacherRepository;
//
//
//	@Autowired
//	private WorkRepository workRepository;
//
//
//	@Autowired
//	private MarkRepository markRepository;
//
//
//	@PostConstruct
//	@Transactional(readOnly = false)
//	public void init() {
//	 Student  abdoulaye= new Student();
//	 abdoulaye.setFirstName("Abdoulaye");
//	 abdoulaye.setLastName("SACKO");
//	 abdoulaye.setEmail("abdoulaye@be.com");
//	 abdoulaye.setPhone("0689124356");
//	 abdoulaye.setBirthDate(new DateTime().withDate(2020, 6, 18).toDate());
//	 abdoulaye.setPassword("abdoulaye");
//	 abdoulaye.setPhoto("photo1");
//	 studentRepository.save(abdoulaye);
//
//
//	 Teacher ousmane= new Teacher();
//	 ousmane.setFirstName("Ousmane");
//	 ousmane.setLastName("Sacko");
//	 ousmane.setEmail("ousmane@gmail.com");
//	 ousmane.setPhone("0751985917");
//	 ousmane.setBirthDate(new DateTime().withDate(2020, 6, 18).toDate());
//	 ousmane.setPassword("ousm12");
//	 ousmane.setPhoto("photo1");
//
//	 Course c = new Course();
//	 c.setName("Java course");
//	 c.setFileName("pdf");
//	 ousmane.addCourse(c);
//
//
//	 Work d = new Work();
//	 d.setFileName("Géometrie");
//     d.setName("abdoulaye");
//	 d.setUploadedDate("26/10/2020");
//	 d.setData();
//	 abdoulaye.addWork(d);
//	 workRepository.save(d);
//
//	 Mark e = new Mark();
//	 e.setMarkedDate("26/10/2020");
//	 e.setMarkValue(12d);
//	 ousmane.addMarks(e);
//	 d.setMark(e);
//	 teacherRepository.save(ousmane);
//	}
//
//	}
//
//
